#!/usr/bin/env python3

# builtin
import sys
import os

# local
from crawler.crawler import Crawler
from crawler.config import Config

# get config 
config = Config(
    os.path.dirname(os.path.realpath(__file__))
).config()

crawler = Crawler(config)
crawler.run()
