#!/usr/bin/env python3

# builtin
import multiprocessing
import os
import random
import signal
import sys

from crawler.worker import Worker

class Crawler:

    def __init__(self, config):
        self.config = config
        self.max_workers = config['settings']['max_workers']
        self.debug = config['settings']['debug']
        signal.signal(signal.SIGINT, self.sigint)

    def run(self):

        # these queues will be used to communicate to and from the workers
        todo = multiprocessing.Queue()
        done = multiprocessing.Queue()

        # global array to make sure we only visit each url once
        visited_urls = set()

        self.workers = {}
        in_progress = {}
        results = []

        # generate initial task (page root), the rest will be generated
        # dynamically by the 

        url = self.config['target']['root']
        task = (url, url)
        todo.put(task)
        in_progress[url] = 1

        print("CTRL-C to EXIT")

        # generate workers
        for i in range(self.max_workers):
            worker = Worker(todo, done, self.config)
            self.workers[worker.name] = worker
            self.workers[worker.name].start()

        while self.workers:
            if not done.empty():
                result = done.get()

                if result[0] in in_progress:
                    del in_progress[result[0]]
                    results.append(result)

                    # re-feed tasks back to the todo queue
                    if result[1]:
                        for link in result[1]:
                            if link not in visited_urls:
                                visited_urls.add(link)
                                todo.put((link, link))
                                in_progress[link] = 1
                        

            if not in_progress:
                # all tasks done, reap children by sending poison pill
                for i in range(self.max_workers):
                    todo.put(None)

                for worker_id in list(self.workers.keys()):
                    if self.workers[worker_id].exitcode is not None:
                        self.workers[worker_id].join()
                        del self.workers[worker_id]
        
    # interrupt signal handler
    def sigint(self, signal, frame):
        for worker_id in list(self.workers.keys()):
            self.pdebug("Terminating worker: " + worker_id)
            if self.workers[worker_id]:
                self.workers[worker_id].terminate()
                del self.workers[worker_id]

        sys.exit(0)

    def __del__(self):
        print("Finished. You can find downloaded images in: %s" % self.config['settings']['target_dir'])
        

    def pdebug(self, message):            
        if self.debug:
            print("MAIN: %s" % (message,))


