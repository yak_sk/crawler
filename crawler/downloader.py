#!/usr/bin/env python3

import urllib.request
import lxml.html
import os
import re
import random
import time

class Downloader:

    def __init__(self, config, url):
        self.config = config
        
        if re.match('^https?', url):
            self.url = url
        else:
            self.url = self.config['target']['protocol'] + '://' + self.config['target']['web'] + url
            

    def get_url(self):
      
        # filter protocol, domains, etc.
        print("get_url pid: "+ str(os.getpid()) + " Getting: " + self.url)

        # is image
        if re.match('^.*\.(jpe?g|gif|png|bmp|tiff)$', self.url, re.IGNORECASE):
            self.download_url_to_file(self.url)
            return []

        # artificial pause 
        try:
            response = urllib.request.urlopen(self.url, timeout=5)
        except:
            return []

        bytes_out = response.read()
        page_string = bytes_out.decode("utf8")
        response.close()

        links = [] 

        try:
            doc = lxml.html.fromstring(page_string)
        except:
            return []

        results = []
        
        for img in doc.xpath('//img/@src'):
            link_out = self.transform_link(img)
            if self.is_subdomain(link_out) and not self.is_ignored(link_out):
                results.append(link_out)
    

        for link in doc.xpath('//a/@href'):
            link_out = self.transform_link(link)
            if self.is_subdomain(link_out) and not self.is_ignored(link_out):
                results.append(link_out)
            
        return results

    def transform_link(self, link):
        #Transform potentially relative urls to absolute urls

        if re.match('^https?', link):
            return link

        if re.match('^/', link):
            domain_part = re.findall('^https?://[^/]+', self.url)
            return ''.join((domain_part[0], link))

        master = self.url.split('/')
        link  = link.split('/')

        while link[0] == '..':
            del link[0]
            del master[-1]

        master.extend(link)
        return '/'.join(master)

    def is_subdomain(self, url):
        if re.match('^https?://[^/]*' + self.config['target']['domain'] + '/?.*$', url):
            return True
        else:
            return False

    def is_ignored(self, url):
        if re.match( '^.*\.(rar|tar|zip|tar.gz|iso|tgz|tar.xz)$', self.url, re.IGNORECASE):
            return True
        else:
            return False

    def download_url_to_file(self, url):
        target_dir = self.config['settings']['target_dir']

        save_to = [ target_dir ]
        save_to.extend(re.sub('^https?://', '', url).split('/'))

        target_file = os.path.join(*save_to)

        if not os.path.isfile(target_file):

            try:
                tmp_file, headers = urllib.request.urlretrieve(url)
            except:
                return

            os.makedirs(os.path.join(target_dir, *save_to[:-1]), exist_ok=True)
            os.rename(tmp_file, target_file)

