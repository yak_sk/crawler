#!/usr/bin/env python3

# builtin
import json
import os

class Config:

    def __init__(self, config_dir):
        self.config_dir = config_dir

    def config(self):
        return json.load(open(os.path.join(self.config_dir, 'config.json')))
