#!/usr/bin/env python3

import multiprocessing
import os
import time
import random

from crawler.downloader import Downloader

class Worker(multiprocessing.Process):

    def __init__(self, input_q, output_q, config):
        multiprocessing.Process.__init__(self)
        self.input_q = input_q
        self.output_q = output_q
        self.mypid = None
        self.config = config
        self.debug = config['settings']['debug']
    
    def run(self):
        self.mypid = os.getpid()

        self.pdebug("Worker %s (pid: %s) started." % (self.name, self.mypid))

        while True:
            next_task = self.input_q.get()

            if next_task is None:
                # Poison pill means shutdown
                self.pdebug('Exiting: Poison pill.')
                break
            
            # process task here
            url_todo = next_task[1]
            
            # wait a random time (in milliseconds)
            wait = random.randint(0, self.config['settings']['max_delay'])
            time.sleep(wait/1000)

            processor = Downloader(self.config, url_todo)
            links = processor.get_url()

            self.output_q.put((url_todo, tuple(links)))

        return

    def pdebug(self, message):
        if self.debug:
            print("%s %s: %s" % (self.name, self.mypid, message))
        
